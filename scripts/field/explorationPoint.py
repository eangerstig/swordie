# Explorer Medal Quest | Is called when player enters certain maps

BEGINNER = [100000000, 100020400, 100040000, 101000000, 101020300, 101040300, 102000000, 102020500, 102030400, 102040200, 103000000, 103020200, 103030400, 103040000, 104000000, 104020000, 106020100, 120000000, 120020400, 120030000]
ELNATH = [200000000, 200010100, 200010300, 200080000, 200080100, 211000000, 211030000, 211040300, 211041200, 211041800]
LUDUSLAKE = [222000000, 222010400, 222020000, 220000000, 220020300, 220040200, 221020701, 221000000, 221030600, 221040400]
UNDERWATER = [230000000, 230010400, 230010200, 230010201, 230020000, 230020201, 230030100, 230040000, 230040200, 230040400]
MULUNG = [251000000, 251010200, 251010402, 251010500, 250010500, 250010504, 250000000, 250010300, 250010304, 250020300]
NIHALDESERT = [261030000, 261020401, 261020000, 261010100, 261000000, 260020700, 260020300, 260000000, 260010600, 260010300]
MINARFOREST = [240000000, 240010200, 240010800, 240020401, 240020101, 240030000, 240040400, 240040511, 240040521, 240050000]
SLEEPYWOOD = [105000000, 105100100, 105010100, 105020100, 105020300, 105030000, 105030100, 105030300, 105030500, 105030500]
# TODO figure out the rest of the maps

BEG_QID = 29005
ELNATH_QID = 29006
LUDUS_QID = 29007
UWATER_QID = 29008
MUL_QID = 29009
NIHAL_QID = 29010
MINAR_QID = 29011
SLEEPY_QID = 29014
# theres technically a level requirement too but fuck that

OSSYR_QID = 29012 	# complete -> elnath, ludus, underwater, mulung, nihal, minar
MAPLE_QID = 29013 	# complete -> vic & ossyria
VIC_QID = 29015 	# complete -> beginner, sleepywood

pFieldID = sm.getFieldID()

# sm.dispose() # broken for now

if pFieldID in BEGINNER:
	sm.handleExplorationQuest("Beginner Explorer", len(BEGINNER), BEG_QID)
elif pFieldID in ELNATH:
	sm.handleExplorationQuest("El Nath Explorer", len(ELNATH), ELNATH_QID)
elif pFieldID in LUDUSLAKE:
	sm.handleExplorationQuest("Ludus Lake Explorer", len(LUDUSLAKE), LUDUS_QID)
elif pFieldID in UNDERWATER:
	sm.handleExplorationQuest("Undersea Explorer", len(UNDERWATER), UWATER_QID)
elif pFieldID in MULUNG:
	sm.handleExplorationQuest("Mu Lung Explorer", len(MULUNG), MUL_QID)
elif pFieldID in NIHALDESERT:
	sm.handleExplorationQuest("Nihal Desert Explorer", len(NIHALDESERT), NIHAL_QID)
elif pFieldID in MINARFOREST:
	sm.handleExplorationQuest("Minar Forest Explorer", len(MINARFOREST), MINAR_QID)
elif pFieldID in SLEEPYWOOD:
	sm.handleExplorationQuest("Sleepywood Explorer", len(SLEEPYWOOD), SLEEPY_QID)
else:
	sm.systemMessage("You're a true pioneer! Alert one of the staff about your discovery to receive a reward..") # 1 vp or some shit

# do victoria island explorer quest
if sm.hasQuestCompleted(SLEEPY_QID) and sm.hasQuestCompleted(BEG_QID) and not sm.hasQuestCompleted(VIC_QID):
	sm.forceCompleteQuest(VIC_QID)

# do ossyria explorer quest
if sm.hasQuestCompleted(ELNATH_QID) and sm.hasQuestCompleted(LUDUS_QID) and sm.hasQuestCompleted(UWATER_QID) and sm.hasQuestCompleted(MUL_QID) and sm.hasQuestCompleted(NIHAL_QID) and sm.hasQuestCompleted(MINAR_QID) and not sm.hasQuestCompleted(OSSYR_QID):
	sm.forceCompleteQuest(OSSYR_QID)

# do maple explorer quest
if sm.hasQuestCompleted(OSSYR_QID) and sm.hasQuestCompleted(VIC_QID) and not sm.hasQuestCompleted(MAPLE_QID):
	sm.forceCompleteQuest(MAPLE_QID)




# idk what the below is for but ima leave it..
if sm.getFieldID() == 105000000 and sm.hasQuest(30004):
    sm.setPlayerAsSpeaker()
    sm.sendNext("Welp, this thing is ancient, but seems to be working. Guess I should head back.")
    sm.warp(910700200, 0) # Root Abyss Quest Line Map
    sm.completeQuest(30004)

elif sm.getFieldID() == 104000000:
    sm.showEffect("Map/Effect.img/maplemap/enter/104000000")

elif sm.getFieldID() == 220080000 and sm.hasQuest(1662):
    sm.chatScript("Enter papulatus.")