# 2433844 - Gold Dragon - consume_2433844

import random

NPC = 9000174 # TODO find real scripting verbiage for this

ITEM = 2433844
REQUIRED = 10

MAGNIFICENT = 2591484 # todo
LEFT = 2591468
RIGHT = 2591475


reqAmt = str(REQUIRED)
itemName = md.itemImageAndName(ITEM)
reward = random.randint(LEFT, RIGHT)

sm.setSpeakerID(NPC)

list = ""
for i in range(LEFT, RIGHT):
    list += md.newLine() + md.itemImageAndName(i)

list = md.purpleText(list)

if not sm.hasItem(ITEM, REQUIRED):
    sm.sendSayOkay("You need at least " + reqAmt + " " + itemName + ".\r\nYou currently only have " + md.itemCount(ITEM) + ".\r\n\r\nWhen you have the required items, you can trade them for one of the following random items:" + list)
    
elif not sm.canHold(reward, 1):
    sm.sendSayOkay("Make sure you have space in your inventory.")
    
elif sm.sendAskYesNo("Would you like to trade " + reqAmt + " " + itemName + md.rn() + "For one of the following items?" + list):
    sm.consumeItem(ITEM, REQUIRED)
    sm.giveItem(reward, 1)
    
else:
    sm.sendSayOkay("Don't waste my time or I may waste yours...") # he's the embodiment of death so he's gotta be scary sounding
