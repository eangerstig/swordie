from net.swordie.ms.constants import BossConstants

# NPC ID: 2020010               | Adobis
# Exists in Field ID: 211042300 | El Nath: The Door To Zakum
# Happy path (no errors) text is GMS-like

EL_NATH = 211000000

offering = {
    0 : BossConstants.ZAKUM_EASY_SPAWN_ITEM,
    1 : BossConstants.ZAKUM_HARD_SPAWN_ITEM
}

text = "Well... Okay. It seems you fulfill the requirements. What would you like to do?\r\n#b"
text += "#L0#Investigate dead mine caves.#l\r\n"
text += "#L1#Explore Zakum Dungeon.#l\r\n"
text += "#L2#Receive an offering for Zakum.#l\r\n"
text += "#L3#Go to El Nath.#l"

response = sm.sendNext(text)

if response == 0: # not GMS-like
    sm.sendPrev("Not completed yet.")
elif response == 1: # not GMS-like
    sm.sendPrev("Not completed yet.")
elif response == 2: # GMS-like
    itemchoice = sm.sendNext("Which Zakum are you making an offering to?\r\n#L0#Easy Zakum#l\r\n#L1#Normal/Chaos Zakum#l")
    if sm.canHold(offering[itemchoice], 1):
        sm.sendNext("You need an offering for Zakum...")
        sm.sendNext("Since I have lots of #i" + str(offering[itemchoice]) + "#.. I'll just give you some. Not good for anything besides offerings anyway.")
        sm.sendNext("Just drop this on the Zakum altar to summon the Zakum.")
        sm.giveItem(offering[itemchoice], 1)
    else:
        sm.sendSayOkay("Please make space in your Etc item inventory for one #i" + str(offering[itemchoice]) + "#.") # unsure if GMS-like
elif response == 3: # not GMS-like
    sm.sendNext("Alrrrrrrighty then, I'll send you on your way.")
    sm.warp(EL_NATH, 9) # portal 9 is outside the chief residence building