# NPC ID: 2020010
# Exists in Field ID: 211000001
# Town: El Nath
# Map: El Nath - Chief's Residence
# Function: Warps to Zakum Quest
# GMS-Like: Yes

response = sm.sendNext("What brings you all the way here?\r\n#b#L0#I want to challenge the Zakum.#l\r\n#L1#Nothing, never mind.#l")

DOOR_TO_ZAKUM = 211042300
ADOBIS = 2030008
LEFT_PORTAL = 2

if response == 0:
    sm.sendNext("Then I will send you to #m" + str(DOOR_TO_ZAKUM) + "# , where #p" + str(ADOBIS) + "# is.")
    sm.warp(DOOR_TO_ZAKUM, LEFT_PORTAL)
# unsure what the other option says 