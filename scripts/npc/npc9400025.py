# NPC ID: 9400025
# Location: Henesys Town
# Lil Murgoth | Cloverologist
# says hello

sm.sendNext("Hello World!\r\nNewLine")      # add new line
sm.sendNext("Hello #h #")                   # show character name
sm.sendNext("Please kill 100 #o100000#'s")  # show mob name
sm.sendNext("#t1102625#")                   # show item name
sm.sendNext("#1102625#")                   # show item name
sm.localEmotion(1, 1000, False)