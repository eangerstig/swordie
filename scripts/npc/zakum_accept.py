# Adobis - Entrance To (Easy/Chaos) Zakum's Altar

from net.swordie.ms.constants import BossConstants
from net.swordie.ms.world.event import BossType

fields = {
BossConstants.ZAKUM_EASY_ENTRANCE : BossType.EasyZakum, # 30 minute timer
BossConstants.ZAKUM_HARD_ENTRANCE : BossType.NormalZakum, # 45 minute timer
BossConstants.ZAKUM_CHAOS_ENTRANCE : BossType.ChaosZakum # 60 minute timer
}

curX = chr.getPosition().getX()

if curX > -1040 and curX < -700:
	fid = sm.getFieldID()
	if sm.sendAskYesNo("Is your party ready? Only the party members in this map will be warped in. They cannot join afterwards."):
		if sm.getParty() is None:
			sm.sendSayOkay("Please create a party before going in.")
		elif not sm.isPartyLeader():
			sm.sendSayOkay("Please have your party leader talk to me if you wish to face Zakum.")
		elif sm.checkParty():
			sm.startBossParty(fields[fid])
else:
	sm.sendSayOkay("What? Come closer, I can't hear you.")