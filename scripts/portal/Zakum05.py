from net.swordie.ms.constants import BossConstants

# todo level checks

fields = [
["#bEasy#k", BossConstants.ZAKUM_EASY_ENTRANCE],
["Normal", BossConstants.ZAKUM_HARD_ENTRANCE],
["#rChaos#k", BossConstants.ZAKUM_CHAOS_ENTRANCE]
]

# Zakum door portal
s = "Which difficulty of Zakum do you wish to fight? \r\n"
i = 0
for entry in fields:
    s += "#L" + str(i) + "#" + entry[0] + "#l\r\n"
    i += 1

sm.setSpeakerID(2030013) # adobis

answer = sm.sendSay(s)

if chr.getLevel() >= 70:
    sm.warp(fields[answer][1], 1)
else:
    sm.sendSayOkay("You must be at least level 70 in order to battle the " + fields[answer][0] + " Zakum.")