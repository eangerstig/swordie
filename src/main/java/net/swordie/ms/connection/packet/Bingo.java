package net.swordie.ms.connection.packet;

import net.swordie.ms.connection.OutPacket;
import net.swordie.ms.handlers.header.OutHeader;

import java.util.List;

public class Bingo {
    // verified in CField_HundredBingo::OnPacket
    // improved on Yuuroido's RZ release

    private final static String[] stageOneMessages = {
            "Hey, wait a minute, yo.", // last message to be shown to players
            "Click the numbers when they pop up!",
            "Press the Bingo button when you have a bingo.",
            "I say Bing! You say Go! Bing! Go! Bing! Go!",
            "Holler if you love Bingo!",
            "The game will begin soon!" // first message to be shown to players
    };

    // sent at the beginning of each stage
    public static OutPacket enterGame(int stage, List<Integer> tiles) {
        OutPacket p = new OutPacket(OutHeader.BINGO_ENTER_GAME);
        p.encodeInt(stage);
        p.encodeInt(stage == 1);
        if (stage == 1) {
            encodeFirstStageInfo(p);
        }
        p.encodeInt(5); // idk what these magic numbers mean
        p.encodeInt(5); // maybe number of rounds?? .__.
        encodeBingoLines(p, tiles);
        return p;
    }

    // encoded only on stage 1
    private static void encodeFirstStageInfo(OutPacket p) {
        p.encodeInt(stageOneMessages.length);
        for (String s : stageOneMessages) {
            p.encodeString(s);
        }
    }

    private static void encodeBingoLines(OutPacket p, List<Integer> tiles) {
        p.encodeInt(tiles.size());
        for (Integer tile : tiles) {
            p.encodeInt(tile);
        }
    }

    // display the next bingo number ball
    public static OutPacket hostNumber(int number, int remaining) {
        OutPacket p = new OutPacket(OutHeader.BINGO_HOST_NUMBER);
        p.encodeInt(number);
        p.encodeInt(remaining);
        return p;
    }

    public static OutPacket hostNumberReady() {
        return new OutPacket(OutHeader.BINGO_HOST_NUMBER_READY);
    }

    // display a character that has won (add to sidebar)
    public static OutPacket addRank(int charId, String charName, int stage, int rank) {
        OutPacket p = new OutPacket(OutHeader.BINGO_ADD_RANK);
        p.encodeInt(1); // operator??
        p.encodeInt(charId);
        p.encodeString(charName);
        p.encodeInt(stage);
        p.encodeInt(rank);
        return p;
    }

    public static OutPacket addRankFail(int charId, String charName) {
        OutPacket p = new OutPacket(OutHeader.BINGO_ADD_RANK);
        p.encodeInt(1);
        p.encodeInt(charId);
        p.encodeString(charName);
        p.encodeInt(0);
        p.encodeInt(0);
        return p;
    }

    public static OutPacket finishRank(int stage, int rank) {
        OutPacket p = new OutPacket(OutHeader.BINGO_FINISH_RANK);
        p.encodeInt(stage);
        p.encodeInt(rank);
        return p;
    }

    public static OutPacket checkNumberAck(int index, int number, List<Integer> tiles) {
        OutPacket p = new OutPacket(OutHeader.BINGO_CHECK_NUMBER_ACK); // when they switch from result to ack //.//
        p.encodeInt(index); // nPos
        p.encodeInt(number); // nNumber
        p.encodeInt(0); // ??
        encodeBingoLines(p, tiles);
        return p;
    }

    public static OutPacket setGameState(int state, int stage) {
        OutPacket p = new OutPacket(OutHeader.BINGO_GAME_STATE);
        p.encodeInt(state);
        p.encodeInt(stage);
        return p;
    }
}
