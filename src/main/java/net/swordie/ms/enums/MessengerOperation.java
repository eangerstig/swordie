package net.swordie.ms.enums;

import java.util.Arrays;

// CUIMessenger::OnPacket : v176 -> 0102E4B0
// KMST leak better source for looking at this prolly, my 176 was mostly corrupted
public enum MessengerOperation {
    Enter(0),
    SelfEnterResult(1),
    Leave(2),
    Invite(3),
    InviteResult(4),
    Blocked(5),
    Chat(6),
    Avatar(7),
    Migrated(8),
    IncLikePointResult(10),
    UserInfo(11),
    Whisper(14),
    Topic(15),
    CreateFailed(16),
    ;

    private byte val;

    MessengerOperation(int val) {
        this.val = (byte) val;
    }

    public byte getVal() {
        return val;
    }

    public static MessengerOperation getByVal(byte val) {
        return Arrays.stream(values()).filter(mrt -> mrt.getVal() == val).findAny().orElse(null);
    }
}
