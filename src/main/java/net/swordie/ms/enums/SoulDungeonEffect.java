package net.swordie.ms.enums;

public enum SoulDungeonEffect {
    Start(14),
    SetSoulResult(15),
    SoulSummon(16),
    SoulTrapUse(17),
    Clear(18),
    ItemEffect(19),
    ;

    int val;

    SoulDungeonEffect(int type) {
        val = type;
    }

    public int getVal() {
        return val;
    }
}
