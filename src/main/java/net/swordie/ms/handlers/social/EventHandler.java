package net.swordie.ms.handlers.social;

import net.swordie.ms.client.Client;
import net.swordie.ms.connection.InPacket;
import net.swordie.ms.handlers.Handler;
import net.swordie.ms.handlers.header.InHeader;
import net.swordie.ms.world.event.InGameEventManager;
import net.swordie.ms.world.event.InGameEventType;
import net.swordie.ms.world.event.bingo.BingoEvent;

// class that handles all the incoming maple in game event packets
public class EventHandler {

    @Handler(op = InHeader.BINGO_CHECK_BINGO)
    public static void handleBingoCheckBingo(Client c, InPacket inPacket) {
        BingoEvent event = (BingoEvent) InGameEventManager.getInstance().getEvent(InGameEventType.Bingo);
        if (event.isActive()) {
            if (event.isOpen()) { // hasn't started yet
                return;
            }
            event.tryBingo(c.getChr());
        } else {
            c.getChr().dispose();
        }
    }

    @Handler(op = InHeader.BINGO_CHECK_NUMBER)
    public static void handleBingoCheckNumber(Client c, InPacket inPacket) {
        BingoEvent event = (BingoEvent) InGameEventManager.getInstance().getEvent(InGameEventType.Bingo);
        if (event.isActive()) {
            if (event.isOpen()) { // hasn't started yet
                return;
            }
            event.tryCircleBoardValue(c.getChr(), inPacket.decodeInt(), inPacket.decodeInt());
        } else {
            c.getChr().dispose();
        }
    }
}
