package net.swordie.ms.scripts;

/**
 * This class will serve as a container for all the functions that return specific scripting markdown text.
 */
public class ScriptMarkdown {

    public final String rn = "\\r\\n";
    public final String pName = "#k #";

    /**
     * Make a bolded string
     * @param toBold string to bold
     * @return bolded string
     */
    public String boldText(String toBold) {
        return "#e" + toBold + "#";
    }

    /**
     * Make a string with blue text
     * @param toBlueify string to make blue
     * @return blue string
     */
    public String blueText(String toBlueify) {
        return "#b"  + toBlueify + "#";
    }

    /**
     * Make a string with red text
     * @param toRedify string to make red
     * @return blue string
     */
    public String redText(String toRedify) {
        return "#r" + toRedify + "#";
    }

    /**
     * Make a string with purple text
     * @param toPurplify string to make purple
     * @return blue string
     */
    public String purpleText(String toPurplify) {
        return "#d" + toPurplify + "#";
    }

    /**
     * Make a string with green text
     * @param toGreenify string to make green
     * @return blue string
     */
    public String greenText(String toGreenify) {
        return "#g" + "#";
    }

    /**
     * Make a string that will display an image of a progress bar
     * @param percentComplete percent of the progress bar to show complete
     * @return string code for a progress bar
     */
    public String progressBar(int percentComplete) {
        return "#B" + percentComplete + "#";
    }

    /**
     * Make a string that will show the user how many of a given item they have
     * @param itemId id of item to count
     * @return string code for item count display
     */
    public String itemCount(int itemId) {
        return "#c" + itemId + "#";
    }

    /**
     * Make a string that will display the map name in-game
     * @param fieldId map id to get name of
     * @return string to send to client
     */
    public String mapName(int fieldId) {
        return "#m" + fieldId + "#";
    }

    /**
     * Make a string that will display the item name in-game
     * @param itemId item id of item to get name of
     * @return string to send to client
     */
    public String itemName(int itemId) {
        return "#t" + itemId + "#";
    }
    /**
     * Make a string that will display an image of an item in-game
     * @param itemId item id of the item to get the image of
     * @return string to send to client
     */
    public String itemImage(int itemId) {
        return "#i" + itemId + "#";
    }

    public String itemImageAndName(int itemId) { return itemImage(itemId) + " " + itemName(itemId); }
    public String itemNameAndImage(int itemId) { return itemName(itemId) + " " + itemImage(itemId); }

    /**
     * Make a string that will display the name of the player
     * @return string to send to client
     */
    public String playerName() {
        return pName;
    }

    /**
     * Make a selection option text string
     * @param optionNumber option number
     * @param optionText clickable text visible to user
     * @return string to send to client
     */
    public String selection(int optionNumber, String optionText) {
        return String.format("#L%d##%s#l", optionNumber, optionText);
    }

    /**
     * Make many selections starting at 0
     * Each selection is on a new line
     * @param optionText all the possible selection options
     * @return string to send to client
     */
    public String selection(String... optionText) {
        StringBuilder retVal = new StringBuilder();
        for (int i = 0; i < optionText.length; i++) {
            retVal.append(selection(i, optionText[i]));
            retVal.append(newLine());
        }
        return retVal.toString();
    }

    /**
     * Creates a string that tells the client to start a new line.
     * @return string to send to client
     */
    public String newLine() {
        return rn;
    }

    /**
     * Make a string that will display a name of the given mod
     * @param mobId id of mob to get name of
     * @return string to send to client
     */
    public String mobName(int mobId) {
        return "#o" + mobId + "#";
    }

    /**
     * Make a string that displays the name of an NPC
     * @param npcId id of npc to get name of
     * @return string to send to client
     */
    public String npcName(int npcId) {
        return "#p" + npcId + "#";
    }

    /**
     * Make a string that displays the name of a skill
     * @param skillId id of skill to display name of
     * @return string to send to the client
     */
    public String skillName(int skillId) {
        return "#q" + skillId + "#";
    }

    /**
     * Make a string that displays the image of a skill
     * @param skillId id of skill to display image of
     * @return string to send to the client
     */
    public String skillImage(int skillId) {
        return "#s" + skillId + "#";
    }

    /**
     * Make a string that shows a wz image
     * @param imageLocation wz location of image to show
     * @return string to send to the client
     */
    public String wzImage(String imageLocation) {
        return "#f" + imageLocation + "#";
    }
}
