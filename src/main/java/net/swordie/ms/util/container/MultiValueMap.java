package net.swordie.ms.util.container;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

// https://stackoverflow.com/questions/35641702/which-java-collection-allows-duplicate-keys
public class MultiValueMap<K,V> {
    private final Map<K, Set<V>> mappings = new HashMap<K,Set<V>>();

    /**
     * Get a set of values associated with the given key.
     * @param key key to get the values for
     * @return a HashSet of the given key's values
     */
    public Set<V> getValues(K key) {
        return mappings.get(key);
    }

    /**
     * Adds a key with a value to the collection.
     *
     * @param key key to add
     * @param value value to add
     * @return true if the set didn't already contain the value
     */
    public Boolean putValue(K key, V value) {
        Set<V> target = mappings.computeIfAbsent(key, k -> new HashSet<V>());
        return target.add(value);
    }

    /**
     * Removes all keys and values from collection.
     */
    public void clear() {
        mappings.clear();
    }

    /**
     * Removes a key from the collection if it exists.
     *
     * @param key key to remove
     */
    public void removeKey(K key) {
        mappings.remove(key);
    }
}
