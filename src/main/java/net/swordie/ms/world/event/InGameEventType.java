package net.swordie.ms.world.event;

public enum InGameEventType {
    PinkZakumBattle,
    RussianRoulette,
    Bingo,
    // more to come...
}
