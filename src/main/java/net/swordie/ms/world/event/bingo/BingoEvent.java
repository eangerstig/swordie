package net.swordie.ms.world.event.bingo;

import net.swordie.ms.Server;
import net.swordie.ms.ServerConfig;
import net.swordie.ms.client.character.BroadcastMsg;
import net.swordie.ms.client.character.Char;
import net.swordie.ms.connection.OutPacket;
import net.swordie.ms.connection.packet.Bingo;
import net.swordie.ms.connection.packet.FieldPacket;
import net.swordie.ms.connection.packet.WvsContext;
import net.swordie.ms.enums.WeatherEffNoticeType;
import net.swordie.ms.handlers.EventManager;
import net.swordie.ms.world.Channel;
import net.swordie.ms.world.event.InGameEvent;
import net.swordie.ms.world.event.InGameEventManager;
import net.swordie.ms.world.event.InGameEventType;
import net.swordie.ms.world.field.ClockPacket;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

// shoutout to Yuuroido from RZ
public class BingoEvent implements InGameEvent {

    private static final Logger log = LogManager.getRootLogger();

    private final int MINIMUM_PARTICIPANTS_TO_START = 1;

    private final int TOTAL_STAGES = 3; // gms-like
    private final int TOTAL_BALLS_PER_STAGE = 50; // gms-like
    private final int TOTAL_WINNERS_PER_STAGE = 30; // gms-like

    private final int ROUND_START_TIME = 15; // seconds
    private final int TIME_BETWEEN_BALLS = 5; // seconds

    public static final int BINGO_BOARD_SIZE = 25;
    public static final int BINGO_MIN_VALUE = 1; // will not work correctly if lower than 1
    public static final int BINGO_MAX_VALUE = 75; // crash if higher than this

    private final String EFFECT_DIR_START = "Map/Effect.img/killing/start"; // prolly not gms-like directory call but images are gms-like
    private final String EFFECT_DIR_FINISH = "Map/Effect.img/killing/finish";
    private final String EFFECT_DIR_STAGE = "Map/Effect.img/killing/stage/"; // add number at the end to denote which stage

    public static final int LOBBY_MAP = 922290000; // Hidden Street : Bingo Waiting Area
    private final int GAME_MAP = 922290100; // Hidden Street : Bingo Game Area
    private final int EXIT_MAP = 922290200; // Hidden Street : Bingo Waiting Area (identical map to lobby??)

    // variables that change only once during the event
    private long startTimeMillis;
    private Channel channelInstance;
    private boolean started = false;
    private boolean active = false;

    // non-looping variables
    private ScheduledFuture startTimer;
    private int currentStage;
    private final List<Integer> allStageBallValues = new ArrayList<>(); // complete list of balls that have been and will be used in current stage
    private Map<Integer, BingoPlayer> participants = new HashMap<>();

    // looping variables, should be reset/changed between each stage
    private BingoStageStateInfo currentStageState;
    private final List<Integer> currentStageRevealedBalls = new ArrayList<>();
    private int currentStageBallIndex; // to keep track of where we're at in the ballstack (hehe)
    private int currentStageWinCount; // number of winners in current stage, resets on stage change

    // constructor is only called once when its loaded into IGEM cache
    public BingoEvent() {
        // make sure board sizes and indices are within usable ranges, if not throw error
        if (BINGO_BOARD_SIZE > BINGO_MAX_VALUE || BINGO_MIN_VALUE < 1) { // retard protection
            throw new IndexOutOfBoundsException(); // idk if this is the right type of error to throw
        }
    }

    /**
     * Called at the start of the event.
     * Used to run the lobby timer and set up certain pseudo-constant variables.
     */
    @Override
    public void doEvent() {
        active = true;
        currentStage = 0; // means players are in lobby and have not started yet
        channelInstance = Server.getInstance().getWorlds().get(0).getChannels().get(0);
        channelInstance.getField(LOBBY_MAP).setDropsDisabled(true); // we set these here instead of BingoEvent() because the maps will be recycled and reset otherwise
        channelInstance.getField(GAME_MAP).setDropsDisabled(true);
        channelInstance.getField(EXIT_MAP).setDropsDisabled(true);

        currentStageState = BingoStageStateInfo.PrepareGame;
        startTimer = EventManager.addEvent(this::run, 6, TimeUnit.SECONDS);
        startTimeMillis = System.currentTimeMillis() + InGameEventManager.REGISTRATION_DURATION_MINS * 60 * 1000;
    }

    private void run() {
        // we dont want to send another ball if there are already max winners
        if (currentStageState == BingoStageStateInfo.GameStage && (currentStageBallIndex >= TOTAL_BALLS_PER_STAGE || currentStageWinCount >= TOTAL_WINNERS_PER_STAGE)) {
            currentStageState = BingoStageStateInfo.getNextStageState(currentStageState);
            log.debug("GameStage ending");
        }

        switch (currentStageState) {
            case PrepareGame: // only called once
                // initialize number cache that is used here and also later passed to BingoPlayer to be used as the random value list
                for (int i = BingoEvent.BINGO_MIN_VALUE; i <= BingoEvent.BINGO_MAX_VALUE; i++) {
                    allStageBallValues.add(i); // WARNING: do not ever clear this, only shuffle it. clearing will throw null pointer exception.
                }
                log.debug(allStageBallValues.size());
                started = true;
                Server.getInstance().getWorldById(ServerConfig.WORLD_ID)
                        .broadcastPacket(WvsContext.broadcastMsg(BroadcastMsg.notice("Event registration has ended!")));

                warpMap(LOBBY_MAP, GAME_MAP);
                resetForNewStage();
                sendNewBoards();
                if (channelInstance.getField(GAME_MAP).getChars().size() < MINIMUM_PARTICIPANTS_TO_START) {
                    endEvent(); // todo let players know what happened
                    return;
                }
                break;
            case PrepareStage: // called once per stage
                if (currentStage > 1) {
                    resetForNewStage();
                }
                break;
            case ShowBingoBoard: // called once per stage
                if (currentStage > 1) { // since we already sent a board in PrepareGame
                    sendNewBoards();
                }
                break;
            case PreGame: // called once per stage
                // todo i think we send some effect packets here
                broadcastPacket(GAME_MAP, Bingo.hostNumber(-1, TOTAL_BALLS_PER_STAGE));
                break;
            case GameStage: // called TOTAL_BALLS_PER_STAGE times per stage
                broadcastPacket(GAME_MAP, Bingo.hostNumberReady()); // idk what this packet does but Yuuroido used it so im adding it also
                int ballNumber = allStageBallValues.get(currentStageBallIndex); // retrieve random uncalled ball from stack
                currentStageBallIndex += 1; // increment index
                currentStageRevealedBalls.add(ballNumber); // add number to called numbers
                log.debug("Showing ball with value " + ballNumber + " for round " + currentStageBallIndex);
                broadcastPacket(GAME_MAP, Bingo.hostNumber(ballNumber, TOTAL_BALLS_PER_STAGE - currentStageBallIndex), 2);
                break;
            case EndStage: // called once per stage
                // todo more map effects here
                if (currentStage >= TOTAL_STAGES) {
                    log.debug("ending event");
                    startTimer = EventManager.addEvent(this::endEvent, currentStageState.getSecondsToNextCall(), TimeUnit.SECONDS); // todo investigate if maybe the warpout timer should be longer
                    return;
                }
                break;
        }
        startTimer = EventManager.addEvent(this::run, currentStageState.getSecondsToNextCall(), TimeUnit.SECONDS);

        // GameStage is handled at the beginning of the method
        if (currentStageState != BingoStageStateInfo.GameStage) {
            if (currentStageState != BingoStageStateInfo.PreGame) {
                broadcastPacket(GAME_MAP, Bingo.setGameState(currentStageState.stageStateId, currentStage));
            }
            currentStageState = BingoStageStateInfo.getNextStageState(currentStageState);
            log.debug("changing game state");
        }

        log.debug("end doStage() with stage: " + currentStageState.toString());
    }

    private void sendNewBoards() {
        for (BingoPlayer p : participants.values()) {
            if (p.getChr() == null) {
                removeParticipant(p);
                continue;
            }

            p.shuffleBoard(); // pass number cache
            p.getChr().write(Bingo.enterGame(currentStage, p.getBingoBoardValues()));
        }
    }

    private void resetForNewStage() {
        currentStage += 1;
        currentStageWinCount = 0;
        currentStageBallIndex = 0;
        currentStageRevealedBalls.clear();
        Collections.shuffle(allStageBallValues); // shuffle numbers
    }

    // called from inpacket handler
    public void tryBingo(Char c) {
        BingoPlayer bp = participants.get(c.getId());
        if (bp == null) {
            return;
        }

        if (!bp.hasWonInCurrentRound() && bp.canWin()) {
            if (currentStageWinCount >= TOTAL_WINNERS_PER_STAGE) {
                broadcastPacket(GAME_MAP, Bingo.addRankFail(c.getId(), c.getName()));
            } else {
                currentStageWinCount += 1;
                participants.get(c.getId()).addWin(currentStageWinCount);
                broadcastPacket(GAME_MAP, Bingo.addRank(c.getId(), c.getName(), currentStage, currentStageWinCount));
            }
        }
    }

    // called from inpacket handler
    public void tryCircleBoardValue(Char c, int index, int number) { // index here is the position of the tile the participant clicked on
        if (!currentStageRevealedBalls.contains(number) || index > BINGO_BOARD_SIZE) {
            c.dispose();
            return;
        }

        log.debug("passed ball existence check with index " + index + " and number " + number);

        BingoPlayer bP = participants.get(c.getId());
        if (bP != null && bP.hasBingoBoardValue(index, number)) {
            bP.circleBoardValue(index);
            bP.getChr().write(Bingo.checkNumberAck(index, number, bP.getWinLines()));
            log.debug("number existed and is circled");
        } else {
            c.dispose();
            c.dbgChatMsg("bP null or does not have bingo board value");
        }
    }

    @Override
    public void endEvent() {
        clear();
        distributeRewards();
        warpMap(GAME_MAP, EXIT_MAP);
        channelInstance.getField(EXIT_MAP).broadcastPacket(WvsContext
                .weatherEffectNotice(WeatherEffNoticeType.WizetAdminThumbsUp_2,
                        "We hope you had fun, come back and see us next time!!",
                        15 * 1000));
    }

    @Override
    public void clear() {
        if (startTimer != null) {
            startTimer.cancel(true);
            startTimer = null;
        }
        active = false;
        started = false;
        currentStage = 0;
        currentStageWinCount = 0;
        currentStageBallIndex = 0;
        allStageBallValues.clear();
        currentStageRevealedBalls.clear();
    }

    private void distributeRewards() {
        // todo
    }

    public int getTimeLeft() {
        return (int)(startTimeMillis - System.currentTimeMillis()) / 1000;
    }

    @Override
    public void joinEvent(Char pChar) {
        if (pChar == null) {
            return; // uh idk
        }
        long timeLeftToStart = (startTimeMillis - System.currentTimeMillis()) / 1000;
        pChar.dbgChatMsg("Time left:" + timeLeftToStart);
        pChar.getQuestManager().addQuest(14284); // QID = HundredBingoReward
        pChar.getQuestManager().getQuestById(14284).setQrValue("R1=0;R2=0;R3=0"); // thx Yuuroido
        participants.put(pChar.getId(), new BingoPlayer(pChar));
        pChar.changeChannelAndWarp((byte) channelInstance.getChannelId(), LOBBY_MAP);
        // sending lobby clock is, as always, triggered by a map script to ensure it arrives to chars that warp and change channel
    }

    @Override
    public void sendLobbyClock(Char c) {
        if (c == null) {
            return;
        }

        if (getTimeLeft() >= 2) { // no need to waste bandwidth if there are only a few seconds remaining before warp-in
            c.write(FieldPacket.clock(ClockPacket.secondsClock(getTimeLeft())));
            c.write(WvsContext.weatherEffectNotice(WeatherEffNoticeType.BearInClothes, // Kemdi??
                    "Get ready for some Bingo-go-go-go!!!", // gay message
                    getTimeLeft() * 1000));
        }
    }

    private void broadcastPacket(int fieldId, OutPacket p, int delaySec) {
        EventManager.addEvent(() -> broadcastPacket(fieldId, p), delaySec, TimeUnit.SECONDS);
    }

    private void broadcastPacket(int fieldId, OutPacket outPacket) {
        for (BingoPlayer bP : participants.values()) {
            if (bP.getChr() == null) {
                removeParticipant(bP);
                continue;
            }
            if (bP.getChr().getFieldID() != fieldId) {
                continue; // in the case of this event we only want to send packets to participants in the given map
            }
            bP.getChr().write(outPacket);
        }
    }

    private void warpMap(int fromField, int toFieldId) {
        for (Char c : channelInstance.getField(fromField).getChars()) {
            warpChar(c, toFieldId);
        }
    }

    private void warpChar(Char c, int toFieldId) {
        c.warp(toFieldId, 0, false); // as always, don't save return map to keep correct return map integrity intact
    }

    private void removeParticipant(BingoPlayer toRemove) {
        // todo distribute rewards
        participants.remove(toRemove.getCharID());
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public boolean isOpen() {
        return !started;
    }

    @Override
    public InGameEventType getEventType() {
        return InGameEventType.Bingo;
    }

    @Override
    public int getEventEntryMap() {
        return LOBBY_MAP;
    }

    @Override
    public boolean charInEvent(int charId) {
        if (!active) {
            return false;
        }

        int map = started ? GAME_MAP : LOBBY_MAP;
        for (Char c : channelInstance.getField(map).getChars()) {
            if (c.getId() == charId) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void onMigrateDeath(Char c) {
        // idk how tf  they would die in this event
        c.warp(EXIT_MAP, 0, false);
    }

    @Override
    public String getEventName() {
        return "Maple Bingo";
    }
}
