package net.swordie.ms.world.event.bingo;

import net.swordie.ms.client.character.Char;
import net.swordie.ms.util.container.Tuple;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.*;

public class BingoPlayer {

    private static final Logger log = LogManager.getRootLogger();

    private final Char c;
    private final int charId;
    private final List<Integer> wins = new ArrayList<>(); // each entry is the rank of one of the player's wins
    private Tuple[] bingoBoard = new Tuple[BingoEvent.BINGO_BOARD_SIZE];
    //private final List<Tuple<Integer, BingoBoardTile>> bingoBoard = new LinkedList<>(); // linked because we rely on indexing features
    private boolean hasWonInCurrentRound = false;
    private boolean canWin = false;

    /**
     * BingoPlayer constructor.
     * Initializes variables and creates the number cache.
     * The number cache is not shuffled here.
     *
     * @param pChar Char object owner
     * @throws NullPointerException if pChar is null
     */
    public BingoPlayer(Char pChar) {
        if (pChar == null) {
            throw new NullPointerException(); // big oops
        }
        c = pChar;
        charId = pChar.getId();
    }

    /**
     * Lazy implementation, doesn't actually shuffle but rather clears and re-creates the board.
     * We create a cache with all possible numbers in order then split it up into sections of 15,
     * shuffle each subset, and pick the first five of each subset to be used on the game board.
     */
    public void shuffleBoard() {
        hasWonInCurrentRound = false;
        canWin = false;
        bingoBoard = new Tuple[BingoEvent.BINGO_BOARD_SIZE];
        List<Integer> tempCache = new ArrayList<>();
        for (int i = BingoEvent.BINGO_MIN_VALUE; i <= BingoEvent.BINGO_MAX_VALUE; i++) {
            tempCache.add(i);
        }

        int rightShift = 0;
        for (int index = 0; index < BingoEvent.BINGO_MAX_VALUE; index += 15) {
            List<Integer> randomSubSet = tempCache.subList(index, index + 15);
            Collections.shuffle(randomSubSet);
            for (int x = 0; x < 5; x++) {
                int subIndex = (x * 5) + rightShift;
                bingoBoard[subIndex] = new Tuple<>(randomSubSet.get(x), false); // fill in 0, 5, 10, 15, 20, increment by one and then loop again
                log.debug("Placing value " + randomSubSet.get(x) + " at position " + subIndex);
            }
            rightShift += 1;
        }
        bingoBoard[12] = new Tuple<>(0, true); // wildcard star value
    }

    /**
     * Creates an ArrayList and populates it with the visible values of the bingo board.
     * This function does not shuffle the board, and will return an empty list if the
     * board has not been shuffled.
     * The index of values will correspond to the value's position on the bingo board.
     *
     * @return bingo board values represented by an indexed ArrayList
     */
    public ArrayList<Integer> getBingoBoardValues() { // sent to player in a packet
        ArrayList<Integer> retVal = new ArrayList<>();
        for (Tuple entry : bingoBoard) { // this shit makes me miss C# var
            retVal.add((Integer) entry.getLeft());
        }
        return retVal;
    }

    /**
     * Called from BingoEvent when a player tries to circle a number that has been rolled.
     * BingoEvent will not call this method if the number has not been rolled.
     * This method only validates the existence of the index passed in the parameter.
     * A packet needs to be sent to notify the client that the number was circled successfully.
     *
     * @param index index on board to circle
     * @throws IndexOutOfBoundsException if index is less than 1 or more than the bingo board size
     * @throws NullPointerException if there is no object at the given index
     */
    public void circleBoardValue(int index) {
        if (index < 0 || index >= BingoEvent.BINGO_BOARD_SIZE) {
            throw new IndexOutOfBoundsException(); // big oops
        }

        if (bingoBoard[index] == null) {
            throw new NullPointerException();
        }

        bingoBoard[index].setRight(true);
        getChr().dbgChatMsg(index + " with visible value " + bingoBoard[index].getLeft() + " set to true");
    }

    /**
     * Check if the player's bingo board contains the given visible value.
     *
     * @param value value to check
     * @return true if value exists in the player's bingo board
     */
    public boolean hasBingoBoardValue(int index, int value) {
        if (bingoBoard[index] == null) {
            throw new NullPointerException();
        }

        if (!(bingoBoard[index].getLeft() instanceof Integer)) {
            throw new NumberFormatException(); // maybe wrong error type but they'll get the picture
        }

        return (int) bingoBoard[index].getLeft() == value;
    }

    private boolean has(int index) {
        return (boolean) bingoBoard[index].getRight();
    }

    public List<Integer> getWinLines() {
        List<Integer> retVal = new ArrayList<>();
        // todo make this pretty
        // horizontal
        if (has(0) && has(1) && has(2) && has(3) && has(4)) {
            retVal.add(0);
        }
        if (has(5) && has(6) && has(7) && has(8) && has(9)) {
            retVal.add(0);
        }
        if (has(10) && has(11) && has(12) && has(13) && has(14)) {
            retVal.add(0);
        }
        if (has(15) && has(16) && has(17) && has(18) && has(19)) {
            retVal.add(0);
        }
        if (has(20) && has(21) && has(22) && has(23) && has(24)) {
            retVal.add(0);
        }
        // vertical
        if (has(0) && has(5) && has(10) && has(15) && has(20)) {
            retVal.add(1);
        }
        if (has(1) && has(6) && has(11) && has(16) && has(21)) {
            retVal.add(1);
        }
        if (has(2) && has(7) && has(12) && has(17) && has(22)) {
            retVal.add(1);
        }
        if (has(3) && has(8) && has(13) && has(18) && has(23)) {
            retVal.add(1);
        }
        if (has(4) && has(9) && has(14) && has(19) && has(24)) {
            retVal.add(1);
        }
        // left top to right bottom diagonal
        if (has(0) && has(6) && has(12) && has(18) && has(24)) {
            retVal.add(2);
        }
        // right top to left bottom diagonal
        if (has(4) && has(8) && has(12) && has(16) && has(20)) {
            retVal.add(3);
        }
        if (!hasWonInCurrentRound) {
            canWin = true;
        }
        return retVal;
    }

    public Char getChr() {
        return c;
    }

    public int getCharID() {
        return charId;
    }

    public List<Integer> getWinRanks() {
        return wins;
    }

    public void addWin(int placement) {
        hasWonInCurrentRound = true;
        wins.add(placement); // we dont care what stage they won on, just the placement they got
    }

    public boolean hasWonInCurrentRound() {
        return hasWonInCurrentRound;
    }

    public boolean canWin() {
        return canWin;
    }

    public boolean isWinner() {
        return wins.size() > 0;
    }
}
