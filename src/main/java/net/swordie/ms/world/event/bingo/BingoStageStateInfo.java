package net.swordie.ms.world.event.bingo;

import java.util.Arrays;

public enum BingoStageStateInfo {
    PrepareGame(1, 15), // only used once, the rest are used once per stage (so 3 times total)
    PrepareStage(2, 3),
    ShowBingoBoard(3, 5),
    PreGame(0, 1), // this is not a real stage that the client needs to be sent
    GameStage(4, 3), // time between balls (add 2 to this number because of how the packet staggering works)
    EndStage(5, 5),
    ;

    int stageStateId;
    int secondsToNextCall;

    BingoStageStateInfo(int stateId, int secToNextCall) {
        stageStateId = stateId;
        secondsToNextCall = secToNextCall;
    }

    public int getStageStateId() {
        return stageStateId;
    }

    public int getSecondsToNextCall() {
        return secondsToNextCall;
    }

    public static BingoStageStateInfo getStageStateById(int stateId) {
        return Arrays.stream(values()).filter(bsi -> bsi.getStageStateId() == stateId)
                .findAny().orElse(null);
    }

    public static BingoStageStateInfo getNextStageState(BingoStageStateInfo currentState) {
        return currentState == EndStage
                ? PrepareStage
                : getStageStateById(currentState.getStageStateId() + 1);
    }
}
